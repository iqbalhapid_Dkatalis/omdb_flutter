import 'package:flutter/material.dart';
import 'package:omdb_flutter/src/core/api/movies_api_service.dart';
import 'package:omdb_flutter/src/core/api/movies_api_service_factory.dart';
import 'package:omdb_flutter/src/core/api/cinemas_api_service.dart';
import 'package:omdb_flutter/src/core/api/cinemas_api_service_factory.dart';
import 'package:omdb_flutter/src/core/app.dart';
import 'package:omdb_flutter/src/core/db/database.dart';
import 'package:omdb_flutter/src/core/utils/network_info.dart';
import 'package:omdb_flutter/src/features/favorite/data/favorite_movies_repository.dart';
import 'package:omdb_flutter/src/features/favorite/data/favorite_movies_repository_impl.dart';
import 'package:omdb_flutter/src/features/popular/data/popular_movies_repository.dart';
import 'package:omdb_flutter/src/features/popular/data/popular_movies_repository_impl.dart';
import 'package:omdb_flutter/src/features/cinemas/data/cinemas_repository.dart';
import 'package:omdb_flutter/src/features/cinemas/data/cinemas_repository_impl.dart';
import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.instance;

void main() {
  getIt.registerLazySingleton<MoviesDao>(() => AppDatabase().moviesDao);

  getIt.registerLazySingleton<MoviesApiService>(() => MoviesApiServiceFactory());

  getIt.registerLazySingleton<CinemasApiService>(() => CinemasApiServiceFactory());

  getIt.registerFactory<FavoriteMoviesRepository>(() => FavoriteMoviesRepositoryImpl(),);

  getIt.registerLazySingleton<NetworkInfo>(() => NetworkInfo());

  getIt.registerFactory<PopularMoviesRepository>(
    () => PopularMoviesRepositoryImpl(),
  );

  getIt.registerFactory<CinemasRepository>(
    () => CinemasRepositoryImpl()
  );

  runApp(App());
}