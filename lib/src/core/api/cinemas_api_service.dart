abstract class CinemasApiService {
  buildClient();

  getNearbyCinemas(double latitude, double longitude);
}