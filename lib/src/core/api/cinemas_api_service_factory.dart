import 'package:dio/dio.dart';
import 'package:omdb_flutter/src/core/api/cinemas_api_service.dart';

class CinemasApiServiceFactory implements CinemasApiService {
  @override 
  buildClient(){
    BaseOptions baseOptions = BaseOptions(baseUrl: "https://maps.googleapis.com",);
    
    return Dio(baseOptions);
  }

  @override 
  getNearbyCinemas(double latitude, double longitude) {
    Dio client = buildClient();
    return client.request(
      "/maps/api/place/nearbysearch/json",
      queryParameters: {
        "location": "$latitude, $longitude",
        "radius":"10000",
        "type":"movie_theater",
        "key":"AIzaSyB-Z94w6SSpcBmubSdEEvdf5Dy6ltujR6Q",
      },
      options: Options(
        method: "GET",
        responseType: ResponseType.plain
      ),
    );
  }

}