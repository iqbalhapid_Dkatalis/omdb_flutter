abstract class MoviesApiService {
  buildClient();

  getPopularMovies(int page);

}