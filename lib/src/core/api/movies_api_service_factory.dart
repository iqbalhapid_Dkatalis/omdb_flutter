import 'package:dio/dio.dart';
import 'package:omdb_flutter/src/core/api/movies_api_service.dart';

class MoviesApiServiceFactory implements MoviesApiService {
  
  @override
  buildClient() {
    BaseOptions baseOptions = BaseOptions(
      baseUrl: "https://api.themoviedb.org",
    );

    return Dio(baseOptions);
  }

  @override
  getPopularMovies(int page) {
    Dio client = buildClient();
    return client.request(
      "/3/discover/movie",
      queryParameters: {
        "sort_by": "popularity.desc",
        "api_key": "a006e16ad6051df6252552d8ade9e529",
        "page" : "$page"
      },
      options: Options(method: "GET", responseType: ResponseType.plain),
    );
  }
}
