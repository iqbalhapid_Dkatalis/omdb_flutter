const BASE_IMAGE_URL = 'http://image.tmdb.org/t/p/';

const SIZE_SMALL = 'small';
const SIZE_MEDIUM = 'medium';
const SIZE_LARGE = 'large';

const POSTER_SIZES = {
  'smallest' : 'w92',
  '$SIZE_SMALL' : 'w154',
  '$SIZE_MEDIUM' : 'w185',
  '$SIZE_LARGE' : 'w342',
  'larger' : 'w500',
  'largest' : 'w780',
  'original' : 'original' 
};