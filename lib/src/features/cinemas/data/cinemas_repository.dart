import 'package:omdb_flutter/src/core/utils/result.dart';
import 'package:omdb_flutter/src/features/cinemas/data/models/cinema_response.dart';

abstract class CinemasRepository{
  Future<Result<CinemaResponse>> getNearbyCinemas(double latitude, double longitude);
}