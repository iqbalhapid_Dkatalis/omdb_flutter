import 'package:omdb_flutter/src/features/cinemas/data/models/cinema.dart';
import 'package:json_annotation/json_annotation.dart';
part 'cinema_response.g.dart';

@JsonSerializable()
class CinemaResponse {
  List<Cinema> results;
    @JsonKey(name: 'error_message')
    String errorMessage;
  CinemaResponse(this.results);

  factory CinemaResponse.fromJson(Map<String, dynamic> json) => _$CinemaResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CinemaResponseToJson(this);
} 