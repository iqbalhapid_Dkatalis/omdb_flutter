import 'package:omdb_flutter/src/core/db/database.dart';
import 'package:omdb_flutter/src/core/utils/result.dart';

abstract class FavoriteMoviesRepository{
  Future<Result> saveMovieToFavorites(Movie movie);

  Future<Result<List<Movie>>> getFavoriteMovies();
}