import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:omdb_flutter/src/features/favorite/domain/favorite_movies_bloc.dart';
import 'package:omdb_flutter/src/features/favorite/domain/favorite_movies_state.dart';
import 'package:omdb_flutter/src/features/favorite/domain/favorite_movies_event.dart';

class FavoritesMoviePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<FavoriteMoviesBloc>(context);
    bloc.dispatch(GetFavoriteMovies());

    return Scaffold(
      body: BlocListener<FavoriteMoviesBloc, FavoriteMoviesState>(
        listener: (context, state) {},
        child: BlocBuilder<FavoriteMoviesBloc, FavoriteMoviesState>(
          builder: (context, state) {
            if (state is FavoriteMoviesLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is FavoriteMoviesLoaded) {
              state.movies.forEach((movie) => {
                print(movie.title)
              });
              return Center(child: Text('Success'),);
            }
            if (state is FavoriteMoviesEmpty) {
              return Center(child: Text('Empty'),);
            }
            return Center(
              child: Text('Something went wrong.'),
            );
          },
        ),
      ),
    );
  }

  _showSnackBar(BuildContext context, String message){
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
      )
    );
  }
}
