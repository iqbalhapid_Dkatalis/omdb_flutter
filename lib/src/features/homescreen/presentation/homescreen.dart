import 'package:flutter/material.dart';
import 'package:omdb_flutter/src/features/homescreen/presentation/homescreen_bottomTab.dart';

class HomeScreen extends StatelessWidget {
  final homeScreen = new HomeScreenBottomTab();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.blueGrey), home: HomeScreenBottomTab());
  }
}

