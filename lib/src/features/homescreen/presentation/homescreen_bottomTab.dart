import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:omdb_flutter/src/features/popular/domain/popular_movies_bloc.dart';
import 'package:omdb_flutter/src/features/cinemas/domain/cinemas_bloc.dart';
import 'package:omdb_flutter/src/features/favorite/domain/favorite_movies_bloc.dart';
import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:omdb_flutter/src/features/popular/presentation/popular_movies_page.dart';
import 'package:omdb_flutter/src/features/cinemas/presentation/cinemas_screen.dart';
import 'package:omdb_flutter/src/features/favorite/presentation/favorite_movies_page.dart';


class HomeScreenBottomTab extends StatefulWidget {
  @override
  _HomeScreenBottomTabState createState() => _HomeScreenBottomTabState();
}

class _HomeScreenBottomTabState extends State<HomeScreenBottomTab> {
  int _currentPage = 0;
  GlobalKey bottomNavigationKey = GlobalKey();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: _titleAppBar(),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.email), onPressed: () {}),
            IconButton(icon: Icon(Icons.notifications), onPressed: () {})
          ],
        ),
        body: Container(
          decoration: BoxDecoration(color: Colors.white),
            child: _getPage(_currentPage),
        ),
        bottomNavigationBar: _bottomTabBar(),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              // globalWidget.createHeader(context),
              // globalWidget.createDrawerItem(icon: Icons.favorite, text: 'wishlist', onTap: (){}),
              // globalWidget.createDrawerItem(icon: Icons.compare_arrows, text: 'transaction',onTap: (){}),
              // globalWidget.createDrawerItem(icon: Icons.mode_edit, text: 'change profile',onTap: (){}),
              // globalWidget.createListTile()
              ],
          ),
        ),
      );
  }
    _titleAppBar(){
      return
      _currentPage==0 ? Text("Logo App") : _searchBar();
    }

    _bottomTabBar(){
      return FancyBottomNavigation(
          barBackgroundColor: Colors.blueGrey,
          circleColor: Colors.white,
          inactiveIconColor: Colors.white,
          activeIconColor: Colors.blueGrey,
          textColor: Colors.white,
          tabs: [
            TabData(
                iconData: Icons.home,
                title: "Home",
                onclick: () {
                  final FancyBottomNavigationState fState =
                      bottomNavigationKey.currentState;
                  fState.setPage(0);
                }),
            TabData(iconData: Icons.search, title: "Search", onclick: () {}),
            TabData(iconData: Icons.favorite, title: "Favourite"),
            TabData(iconData: Icons.place, title: "Cinemas"),
          ],
          initialSelection: 0,
          key: bottomNavigationKey,
          onTabChangedListener: (position) {
            setState(() {
              _currentPage = position;
            });
          },
        );
    }

    _getPage(int page) {
    switch (page) {
      case 0:
        return BlocProvider(
          builder: (context) => PopularMoviesBloc(),
          child: PopularMoviesPage());
      case 1:
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("This is the search page"),
          ],
        );
      case 2:
        return BlocProvider<FavoriteMoviesBloc>(
      builder: (context) => FavoriteMoviesBloc(),
      child: FavoritesMoviePage(),
      );
      default:
        return BlocProvider(
          builder: (context) => CinemasBloc(),
          child: CinemasPage(),
        );
    }
  }

  _searchBar(){
      return TextField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, color: Colors.white),
                hintText: "need something?",
                hintStyle: TextStyle(color: Colors.white, fontSize: 14),
                contentPadding: EdgeInsets.all(12),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(50),
                ),
              ),
        );
    }
}