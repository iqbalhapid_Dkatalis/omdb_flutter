import 'package:flutter/material.dart';

class FormLogin extends StatelessWidget {
  const FormLogin({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 2,
      child: Stack(
        children: <Widget>[
          ///container background
          Container(
            height: MediaQuery.of(context).size.height,
            color:  Color.fromRGBO(178, 221, 255, 10),
          ),

          Container(
          height: MediaQuery.of(context).size.height / 2,
          ),
          _buildHeaderForm(context),
        ],
      ),
    );
  }

  _buildHeaderForm(BuildContext context) {
    return Positioned(
      top: MediaQuery.of(context).size.height / 5.6,
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Card(
          margin: EdgeInsets.all(20.0),
          elevation: 8,
          child: Padding(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Text(
                    "Login",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                    ),
                  ),
                ),
                TextField(
                  decoration: InputDecoration(
                      suffixIcon: Icon(
                        Icons.security,
                        color: Colors.pink[200],
                      ),
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.pinkAccent)),
                      labelText: "SecureToken",
                      labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
