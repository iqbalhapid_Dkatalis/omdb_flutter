import 'package:flutter/material.dart';
import 'package:omdb_flutter/src/features/loginscreen/presentation/loginbutton.dart';
import 'package:omdb_flutter/src/features/loginscreen/presentation/loginform.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: MediaQuery.of(context).orientation == Orientation.landscape
            ? SingleChildScrollView(child: ContentArea())
            : ContentArea());
  }
}

class ContentArea extends StatelessWidget {
  const ContentArea({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[FormLogin(), ButtonLogin()],
    );
  }
}


