import 'package:bloc/bloc.dart';
import 'package:omdb_flutter/src/features/moviedetail/domain/movie_details_event.dart';
import 'package:omdb_flutter/src/features/moviedetail/domain/movie_details_state.dart';

class MovieDetailsBloc extends Bloc<MovieDetailsEvent, MovieDetailsState> {
  
  @override 
  MovieDetailsState get initialState => ShowMovieDetails();

  @override 
  Stream<MovieDetailsState> mapEventToState(MovieDetailsEvent event) {
    return null;
  }
}
