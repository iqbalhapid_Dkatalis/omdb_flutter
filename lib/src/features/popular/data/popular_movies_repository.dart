import 'package:omdb_flutter/src/core/utils/result.dart';
import 'package:omdb_flutter/src/features/popular/data/models/popular_movies_response.dart';
import 'package:omdb_flutter/src/core/db/database.dart';
import 'models/movie.dart';
abstract class PopularMoviesRepository{
  
  Future<Result<PopularMoviesResponse>> getPopularMovies();

}