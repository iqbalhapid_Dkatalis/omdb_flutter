import 'dart:convert';

import 'package:omdb_flutter/src/core/api/movies_api_service.dart';
import 'package:omdb_flutter/src/core/utils/network_info.dart';
import 'package:omdb_flutter/src/core/utils/result.dart';
import 'package:omdb_flutter/src/features/popular/data/models/popular_movies_response.dart';
import 'package:omdb_flutter/src/features/popular/data/popular_movies_repository.dart';
import 'package:get_it/get_it.dart';

class PopularMoviesRepositoryImpl implements PopularMoviesRepository{
  
  NetworkInfo _networkInfo;
  MoviesApiService _moviesApiService;

  int pageNumber = 1;

  PopularMoviesRepositoryImpl() {
    _networkInfo = GetIt.instance.get<NetworkInfo>();
    _moviesApiService = GetIt.instance.get<MoviesApiService>();
  }
  
  @override 
  Future<Result<PopularMoviesResponse>> getPopularMovies() async{
    
    bool isConnected = await _networkInfo.isConnected();

    if(isConnected){
    
      try {
        final response = await _moviesApiService.getPopularMovies(pageNumber);
        pageNumber++;
        
        var parsed = json.decode(response.data);
        var model = PopularMoviesResponse.fromJson(parsed);

        return Result(success: model);
    
      } catch(error){
        print(error.toString());
        return Result(error: ServerError());
      }
    }else{
      return Result(error : NoInternetError());
    }
  }  
}