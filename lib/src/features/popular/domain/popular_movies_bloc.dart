import 'package:omdb_flutter/src/core/utils/result.dart';
import 'package:omdb_flutter/src/features/favorite/data/favorite_movies_repository.dart';
import 'package:omdb_flutter/src/features/popular/data/popular_movies_repository.dart';
import 'package:omdb_flutter/src/features/popular/domain/popular_movies_event.dart';
import 'package:omdb_flutter/src/features/popular/domain/popular_movies_state.dart';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';

class PopularMoviesBloc extends Bloc<PopularMoviesEvent, PopularMoviesState> {
  PopularMoviesRepository _popularMoviesRepository;
  FavoriteMoviesRepository _favoriteMovieRepository;

  bool hasReachedEndOfResults = false;

  PopularMoviesBloc() {
    _popularMoviesRepository = GetIt.instance.get<PopularMoviesRepository>();
    _favoriteMovieRepository = GetIt.instance.get<FavoriteMoviesRepository>();
  }

  @override
  PopularMoviesState get initialState => PopularMoviesLoading();

  @override
  Stream<PopularMoviesState> mapEventToState(PopularMoviesEvent event) async* {
    if (event is FetchPopularMovies) {
      //print('Fetch movies Triggered');

      final results = await _popularMoviesRepository.getPopularMovies();

      if (results.success != null) {
        //print('Success = ' + results.success.results[0].title);
        int nextPage = results.success.page + 1;
        int totalPages = results.success.totalPages;

        if (nextPage == totalPages) hasReachedEndOfResults = true;
        yield PopularMoviesLoaded(results.success.results);
      } else {
        if (results.error is NoInternetError) {
          //print('There is no Internet');
          yield PopularMoviesNoInternet();
        }

        if (results.error is ServerError) {
          //print('server error');
          yield PopularMoviesServerError();
        }
      }
    }

    if (event is SavePopularMovie) {
      final result = await _favoriteMovieRepository.saveMovieToFavorites(event.movie);
      if (result.success != null) {
          print(event.movie.title + ' inserted');
        } else {
          print('error');
        }
    }

  }
}
