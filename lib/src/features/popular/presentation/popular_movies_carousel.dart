import 'package:flutter/material.dart';
import 'package:omdb_flutter/src/core/utils/image_constants.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:omdb_flutter/src/features/popular/data/models/movie.dart';
import 'package:omdb_flutter/src/core/db/database.dart';
import 'package:omdb_flutter/src/features/popular/presentation/popular_movies_list.dart';


class BuildCarousel extends PopularMoviesList {
  final List<Movie> movies;
  BuildCarousel(this.movies) : super(movies);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 25),
      margin: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height / 50),
      child: CarouselSlider(
        items: <Widget>[
          _buildImageContent(context, 3),
          _buildImageContent(context, 5),
          _buildImageContent(context, 7),
          _buildImageContent(context, 9),
          _buildImageContent(context, 11),
        ].map((i) {
          return Builder(
            builder: (BuildContext context) {
              return Stack(
                children: <Widget>[
                  i,
                ],
              );
            },
          );
        }).toList(),
        aspectRatio: 2,
        viewportFraction: 0.35,
        initialPage: 0,
        enableInfiniteScroll: true,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        pauseAutoPlayOnTouch: Duration(seconds: 10),
        enlargeCenterPage: true,
        onPageChanged: null,
        scrollDirection: Axis.horizontal,
      ),
    );
  }

  _buildImageContent(BuildContext context, int i) {
    return Container(
          child: Stack(
            children: <Widget>[
              Image.network(
                BASE_IMAGE_URL + POSTER_SIZES[SIZE_MEDIUM] + movies[i].posterPath,
                fit: BoxFit.cover,
                width: MediaQuery.of(context).size.width / 3,
              ),
              _buildOverlay(
                  context: context,
                  title: movies[i].originalTitle,
                  year: movies[i].releaseDate,
                  genre: movies[i].title),
            ],
          ),
        );
  }

  _buildOverlay(
      {BuildContext context, String title, String year, String genre}) {
    return InkWell(
      child: Container(
        height: MediaQuery.of(context).size.width / 2,
        width: MediaQuery.of(context).size.width / 3,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [
              0.5,
              0.8,
            ],
            colors: [
              Colors.black.withOpacity(0.1),
              Colors.black.withOpacity(0.7),
            ],
          ),
        ),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
              padding: const EdgeInsets.only(left: 0.0, bottom: 10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.yellow, fontSize: 12),
                  ),
                  Text(
                    year,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.yellow, fontSize: 12),
                  ),
                  Text(
                    genre,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.yellow, fontSize: 12),
                  )
                ],
              )),
        ),
      ),
      onTap: () {
        print("carousel item tapped");
      },
    );
  }
}
