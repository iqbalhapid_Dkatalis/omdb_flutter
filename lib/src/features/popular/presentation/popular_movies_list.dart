import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:omdb_flutter/src/core/utils/image_constants.dart';
import 'package:omdb_flutter/src/features/moviedetail/domain/movie_details_bloc.dart';
import 'package:omdb_flutter/src/features/moviedetail/presentation/movie_details.dart';
import 'package:omdb_flutter/src/core/db/database.dart';
import 'package:omdb_flutter/src/features/popular/presentation/popular_movies_carousel.dart';
import 'package:omdb_flutter/src/features/popular/domain/popular_movies_bloc.dart';
import 'package:omdb_flutter/src/features/popular/domain/popular_movies_event.dart';

class PopularMoviesList extends StatelessWidget {
  final List<Movie> movies;
  final ScrollController _scrollController = ScrollController();

  PopularMoviesList(this.movies);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PopularMoviesBloc>(context);
    return Container(
        color: Colors.grey[300],
        child: Stack(
          children: <Widget>[
            BuildCarousel(movies),
            SizedBox.expand(
              child: DraggableScrollableSheet(
                  expand: true,
                  maxChildSize: 1,
                  initialChildSize: 0.5,
                  builder: (BuildContext context,
                      ScrollController scrollController) {
                    return NotificationListener<ScrollNotification>(
                      onNotification: (notification) =>
                          _handleScrollNotification(notification, bloc),
                      child: Container(
                        child: GridView.builder(
                          itemCount: _calculateItemCount(bloc),
                          controller: _scrollController,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            childAspectRatio: 2 / 3,
                          ),
                          itemBuilder: (context, position) {
                            if (position >= movies.length) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            return BuildListTile(movies[position], bloc);
                          },
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ));
  }

  int _calculateItemCount(PopularMoviesBloc bloc) {
    if (bloc.hasReachedEndOfResults) {
      return movies.length;
    } else {
      return movies.length + 1;
    }
  }

  bool _handleScrollNotification(
      ScrollNotification notification, PopularMoviesBloc bloc) {
    if (notification is ScrollEndNotification && !bloc.hasReachedEndOfResults) {
      bloc.dispatch(FetchPopularMovies());
    }
    return false;
  }
}

class BuildListTile extends StatelessWidget {
  final Movie movie;
  final PopularMoviesBloc bloc;

  BuildListTile(this.movie, this.bloc);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) {
            return BlocProvider(
              builder: (context) => MovieDetailsBloc(),
              child: MovieDetails(movie: movie),
            );
          },
        ),
      ),
      child: Card(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            //_buildPoster(movie.posterPath),
            Hero(tag: 'poster_${movie.posterPath}',
            child: _buildPoster(movie.posterPath),),
            Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () => bloc.dispatch(SavePopularMovie(movie)),
                  child: Padding(
                      padding: EdgeInsets.only(right: 5.0, top: 5.0),
                      child: Icon(Icons.favorite_border,
                          color: Colors.white, size: 25.0)),
                ))
          ],
        ),
      ),
    );
  }

  Widget _buildPoster(String posterPath) {
    if (posterPath == null) {
      return Image.asset('assets/images/placeholder.png');
    } else {
      return FadeInImage.assetNetwork(
        placeholder: 'assets/images/placeholder.png',
        image: BASE_IMAGE_URL + POSTER_SIZES[SIZE_MEDIUM] + movie.posterPath,
        fit: BoxFit.cover,
      );
    }
  }
}
