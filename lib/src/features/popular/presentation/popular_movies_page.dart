import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:omdb_flutter/src/core/db/database.dart';

import 'package:omdb_flutter/src/features/popular/domain/popular_movies_bloc.dart';
import 'package:omdb_flutter/src/features/popular/domain/popular_movies_event.dart';
import 'package:omdb_flutter/src/features/popular/domain/popular_movies_state.dart';
import 'package:omdb_flutter/src/features/popular/presentation/popular_movies_list.dart';

class PopularMoviesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PopularMoviesBloc>(context);

    bloc.dispatch(FetchPopularMovies());

    List<Movie> movies = List<Movie>();
    return Scaffold(
      body: BlocListener<PopularMoviesBloc, PopularMoviesState>(
        listener: (context, state) {
          if (state is PopularMoviesNoInternet) {
            _showSnackBar(context, "check your internet connection");
          }
          if (state is PopularMoviesServerError) {
            _showSnackBar(context, "Something went wrong with the server");
          }
        },
        child: BlocBuilder<PopularMoviesBloc, PopularMoviesState>(
          builder: (context, state) {
            if (state is PopularMoviesLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is PopularMoviesLoaded) {
              movies.addAll(state.movies);
              return PopularMoviesList(movies);
            }
            if (state is PopularMoviesNoInternet && movies.isEmpty) {
            return Center(
              child: RaisedButton.icon(
                onPressed: () => bloc.dispatch(FetchPopularMovies()),
                icon: Icon(Icons.refresh),
                label: Text('Try again'),
              ),
            );
            }
            return Center( child: Text("Something went wrong"),);
          },
        ),
      ),
    );
  }

  _showSnackBar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }
}
