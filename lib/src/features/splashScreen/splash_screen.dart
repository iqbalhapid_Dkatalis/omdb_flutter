import 'dart:async';
import 'package:flutter/material.dart';
//import 'package:omdb_flutter/src/features/loginscreen/presentation/loginscreen.dart';
import 'package:omdb_flutter/src/features/homescreen/presentation/homescreen.dart';

const int _SPLASH_DURATION = 5;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    loadData();
    super.initState();
  }

  loadData() async {
    return new Timer(Duration(seconds: _SPLASH_DURATION), onDoneLoading);
  }

  onDoneLoading() async {
    Navigator.pushReplacement(context,
        new MaterialPageRoute(builder: (context) => new HomeScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            image: AssetImage(
              'assets/images/dots.gif',
            ),
          )),
      child: Container(
        alignment: Alignment.bottomCenter,
        margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height / 5),
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
        ),
      ),
    );
  }
}
